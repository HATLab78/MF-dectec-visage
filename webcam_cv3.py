import cv2
import time
import random

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)

video_capture = cv2.VideoCapture(0)
#video_capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 1280)
#video_capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 720)

anterior = 0

def addimage(frame, image, x, y, w, h):
    s_img = cv2.imread(image, -1)
    s_img = cv2.resize(s_img, (int(w), int(h)))
    x_offset = x
    y_offset = y
    for c in range(0, 3):
        frame[y_offset:y_offset+s_img.shape[0], x_offset:x_offset+s_img.shape[1], c] = s_img[:,:,c] * (s_img[:,:,3]/255.0) + frame[y_offset:y_offset+s_img.shape[0], x_offset:x_offset+s_img.shape[1], c] * (1.0 - s_img[:,:,3]/255.0)

    return frame


def addphoto(frame, img2, x, y):
    img1 = frame
    img2 = cv2.resize(img2, (300, 150))

    # I want to put logo on top-left corner, So I create a RO
    rows, cols, channels = img2.shape
    roi = img1[x:rows, y:cols]
    # Now create a mask of logo and create its inverse mask also
    img2gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(img2gray, 10, 255, cv2.THRESH_BINARY)
    mask_inv = cv2.bitwise_not(mask)
    # Now black-out the area of logo in ROI
    img1_bg = cv2.bitwise_and(roi, roi, mask=mask_inv)
    # Take only region of logo from logo image.
    img2_fg = cv2.bitwise_and(img2, img2, mask=mask)
    # Put logo in ROI and modify the main image
    dst = cv2.add(img1_bg, img2_fg)
    img1[x:rows, y:cols] = dst


chapeau = "images/chapeau3.png"
logo = "images/logo.png"
nbphoto = 9

currenttime= time.time()
oldtime = currenttime + 15
photo1 = cv2.imread('photo/IMG_1.jpg')
while True:
    if not video_capture.isOpened():
        print('Unable to load camera.')
        time.sleep(5)
        pass

    # Capture frame-by-frame
    ret, frame = video_capture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30)
    )
    currenttime = time.time()

    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        try:
            #addimage(frame, chapeau, x+(w/2), y-(h/2),  w/2, h/2) 
            addimage(frame, chapeau, int(x+(w/2)), int(y-(h/2)),  int(w/2) , int(h/2))
            # Display the resulting frame$
            if currenttime > oldtime:
                oldtime = time.time() + 5
                photo1 = cv2.imread('photo/IMG_' + str(random.randint(1, nbphoto)) + '.jpg')
            
            height1, width1, channels = photo1.shape
            addphoto(frame, photo1, 0, 0)
        except BaseException as e:
            print(e)
            pass

    frame = cv2.flip(frame, 1)
    height, width, channels = frame.shape
    #cv2.putText(frame, "STAND HATLAB", (10, height - 10), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 255, 0))

    addimage(frame, logo, 10, height - 175,  width-10, (height-10)/3)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


    cv2.namedWindow("Video", cv2.WND_PROP_FULLSCREEN)
    #cv2.setWindowProperty("Video", cv2.WND_PROP_FULLSCREEN, cv2.CV_WINDOW_FULLSCREEN)
    cv2.imshow('Video', frame)

# When everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
